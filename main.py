import pandas as pd

from backtest_utilities import PerformanceMatrices

df = pd.read_csv('./dataset/hsif_1min_20180726_20200725.csv')
df.time_key = pd.to_datetime(df.time_key)

df.index = pd.to_datetime(df.loc[:, 'time_key'])

df.drop_duplicates(keep=False, inplace=True)
df.sort_index(inplace=True)

df_daytime = df.between_time(start_time='09:16:00', end_time='16:30:00', include_start=True, include_end=True)

# df_testing = df_daytime[:'2018-09-26']
df_testing = df_daytime.copy()

p = pd.DataFrame(data=0.0, index=pd.to_datetime(df_testing.index.date).drop_duplicates(keep='first'),
                 columns=['profit_and_loss', 'count'])

for i in range(30, df_testing.__len__()-1):

    if df_testing.iloc[i]['time_key'].date() != df_testing.iloc[i+1]['time_key'].date():
        continue

    # if df_testing.iloc[i - 1]['open'] < df_testing.iloc[i - 1]['close'] and \
    #         df_testing.iloc[i-1]['high'] < df_testing.iloc[i]['open'] < df_testing.iloc[i]['close']:
    #     profit_and_loss = df_testing.iloc[i+1]['close'] / df_testing.iloc[i+1]['open'] - 1
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'profit_and_loss'] += profit_and_loss
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'count'] += 1

    # if df_testing.iloc[i - 1]['open'] > df_testing.iloc[i - 1]['close'] and df_testing.iloc[i - 1]['low'] > df_testing.iloc[i]['high'] and df_testing.iloc[i]['open'] > df_testing.iloc[i]['close']:
    #     profit_and_loss = df_testing.iloc[i+1]['close'] - df_testing.iloc[i+1]['open']
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'profit_and_loss'] += profit_and_loss * 50
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'count'] += 1

    # if df_testing.iloc[i]['high'] == df_testing.iloc[i]['open'] and df_testing.iloc[i]['low'] == df_testing.iloc[i]['close']:
    #     profit_and_loss = df_testing.iloc[i + 1]['close'] - df_testing.iloc[i + 1]['open']
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'profit_and_loss'] += (-profit_and_loss * 50)
    #     p.loc[f'{df_testing.iloc[i+1]["time_key"].date()}', 'count'] += 1

    if df_testing.iloc[]

p.to_csv('./hs_5.csv')
# pf = PerformanceMatrices(p['profit_and_loss'])
# pf.performance_plot()
